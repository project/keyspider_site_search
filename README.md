Keyspider Site Search
=====

The Keyspider Search Drupal module replaces Drupal's default search experience with a more accurate and customizable one.

Requirements
-----

You need a Keyspider account[Keyspider](https://www.keyspider.io), If you don't have an account yet, sign up for a free 14-day trial and try out all the features without any restriction.

Setup
-----

### Step 1: Download the Keyspider Search module and install it into your Drupal website as you would any other module.

### Step 2: Once installed, You could see the Keyspider Search Config in the configuration admin menu. Enter your API Key, Organization ID, and search page slug in the input box and click Save button.



### Step 3: Implement search in your website.

i) Create a search page: 

Go to content in Drupal admin.
Add a new basic page and use the same slug you used above configuration.

ii) Assign Keyspider search blocks
 
Go to Structure and Block layout

In the content block, place “Keyspider search page block”
In the sidebar block or header, place “Keyspider search widget block”



Open your search page in the frontend and start using intelligent search. That's it your site is ready to search empowered by Keyspider search.

Read the Keyspider site search Drupal guide for more details.

[Keyspider Site Search Quick Start Guide](https://docs.keyspider.io/docs/drupal-cms/)



Features 
-----

No coding: Create a new page with search capabilities after installing the Keyspider Search plugin. Works with your theme’s existing style guide.

Cloud-based: All of your documents are securely stored, backed up, and searchable in the cloud.

Automatic/Manual update: Automatically WordPress content would be updated in the search engine based on your crawler update settings. The manual recrawl option allows you to instantly crawl new, edit, deleted pages. It will immediately reflect in the updated results when a relevant search is made.

Multiple Data Source: Whether your content is in the form of a webpage, XML, or PDF the crawler will index and display them on the search result page.

Mapping: The specific rules in mapping allows you to customize the appearance of the search result page data to fit your preference.

Typeahead search: Autocomplete suggestions based on titles, taxonomies, plugins data, and custom meta-data as you enter.

Result Re-ranking: Customize the search output of specific queries using a simple drag and drop interface. The feature will enable you to determine the ranking criteria and improve the visibility of relevant content.

Synonyms: Create associations between search terms by matching different variations and vernaculars surrounding your website keywords. The feature allows you to understand the user intent and provide results synonymous with their query.

User Role: Define the extent of access to specific members within the team.