<?php

namespace Drupal\Keyspider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a block for Keyspider search page.
 *
 * @Block(
 *  id = "keyspider_search_page_block",
 *  admin_label = @Translation("Keyspider search page block"),
 * )
 */
class SearchPageBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = array();

    $config = $this->getConfiguration();
      
    $admin_settings = \Drupal::config('keyspider.settings');
    $api_key = $admin_settings->get('ks_api_key');
    $organization_id = $admin_settings->get('ks_organization_id');
    $search_page = $admin_settings->get('ks_search_page');
    
    $build['#attached']['drupalSettings']['keyspider']['ks_api_key'] = $api_key;
    $build['#attached']['drupalSettings']['keyspider']['ks_organization_id'] = $organization_id;
    $build['#attached']['drupalSettings']['keyspider']['ks_search_page'] = $search_page;
    $build['#markup'] = '<div class="dashboard-search"></div>
                        <div class="dashboard-search-results"></div>
                        <div class="dashboard-search-pagination"></div>';
    return $build;
  }
}