<?php

namespace Drupal\Keyspider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a block for a inline search form.
 *
 * @Block(
 *  id = "keyspider_search_widget_block",
 *  admin_label = @Translation("Keyspider search widget block"),
 * )
 */

class SearchWidgetBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */

    public function build() {
      $build = array();

      $config = $this->getConfiguration();
      
      $admin_settings = \Drupal::config('keyspider.settings');
      $api_key = $admin_settings->get('ks_api_key');
      $organization_id = $admin_settings->get('ks_organization_id');
      $search_page = $admin_settings->get('ks_search_page');

      $build['#attached']['drupalSettings']['keyspider']['ks_api_key'] = $api_key;
      $build['#attached']['drupalSettings']['keyspider']['ks_organization_id'] = $organization_id;
      $build['#attached']['drupalSettings']['keyspider']['ks_search_page'] = $search_page;
      $build['#markup'] = '<form class="form-inline my-2 my-lg-0">
                            <div class="dashboard-search-only"></div>
                          </form>';
      return $build;
  }
}