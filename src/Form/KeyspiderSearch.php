<?php

namespace Drupal\keyspider\Form;

use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Form\ConfigFormBase;

/**
 * Smooth scroll.
 */
class KeyspiderSearch extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'keyspider';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'keyspider.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  
    $config = $this->config('keyspider.settings');
    $form['ks_api_key'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Enter your API key'),
      '#default_value' => $config->get('ks_api_key'),
    );

    $form['ks_organization_id'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Organization ID'),
      '#description' => $this->t('Enter your organization ID'),
      '#default_value' => $config->get('ks_organization_id'),
    );
    $form['ks_search_page'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Search page'),
      '#description' => $this->t('Enter your search page relative URL'),
      '#default_value' => $config->get('ks_search_page'),
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('ks_api_key'))) {
      $form_state->setErrorByName('ks_api_key', $this->t('Please provide the API key.'));
    }
    if (empty($form_state->getValue('ks_organization_id'))) {
      $form_state->setErrorByName('ks_organization_id', $this->t('Please provide the organization ID.'));
    }
    if (empty($form_state->getValue('ks_search_page'))) {
      $form_state->setErrorByName('ks_search_page', $this->t('Please provide relative URL of the search page.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('ks_api_key');
    $organization_id = $form_state->getValue('ks_organization_id');
    $search_page = $form_state->getValue('ks_search_page');

    $config = $this->config('keyspider.settings');
      // Set the submitted values in configuration settings yml.
      $config->set('ks_api_key', $api_key);
      $config->set('ks_organization_id', $organization_id);
      $config->set('ks_search_page', $search_page);
      $config->save();
    parent::submitForm($form, $form_state);
  }

}
