jQuery( document ).ready(function() {
  var api_key = drupalSettings.keyspider.ks_api_key;
  var org_id = drupalSettings.keyspider.ks_organization_id;
  var search_page = drupalSettings.keyspider.ks_search_page;
  var search_url = window.location.origin + drupalSettings.path.baseUrl + search_page;

  if (window.location.href.indexOf(search_page) > -1) {
    initiateSearchPage();
    initiateSearchWidget();
  }else{
    initiateSearchWidget();
  }
  
  function initiateSearchPage() {
    window.searchInit && 
    window.searchInit(
      api_key, 
      org_id, 
      search_url,
      "q"
    );
  };

  function initiateSearchWidget() {
    window.searchOnlyInit &&
    window.searchOnlyInit(
      api_key,
      org_id,
      search_url,
      "q"
    );
  };
  

});